// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//include what you use
#include "GameFramework/Actor.h"
#include "Engine/World.h"
//--

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TerminalContolled.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SUPERNOVA_API UTerminalContolled : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTerminalContolled();
	virtual void Open();
	virtual void Close();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:

UPROPERTY(EditAnywhere)
bool IsOpened = false;

UPROPERTY(EditAnywhere)
FVector OpenLocation = FVector(0.0f, 0.0f, 0.0f);

FVector ClosedLocation = FVector(0.0f, 0.0f, 0.0f);
		
};
