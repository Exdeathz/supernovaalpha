// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//Include what you use
#include "Misc/FileHelper.h"
#include "Misc/Paths.h"
#include "kismet/GameplayStatics.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "GameFramework/Pawn.h"
#include "Engine/StaticMeshActor.h"
#include "Templates/UniquePtr.h"
#include <functional>
#include "TerminalContolled.h"
//--

#include "CoreMinimal.h"
#include "Cartridge.h"
#include "OSCartridge.generated.h"

/**
 * 
 */

typedef void (UOSCartridge::*CommandPtr)(const FString &Args);

UCLASS()
class SUPERNOVA_API UOSCartridge : public UCartridge
{
	GENERATED_BODY()

	public:
	virtual void BeginPlay() override;
	virtual void OnInput(const FString& Input) override;

	private:
	virtual void ExecCommand(const FString& Command, const FString& Args);

	virtual void ParseCommand(const FString& Input);
	virtual void Command_OpenDoor(const FString &Input);
	virtual void List(const FString& Args);

	virtual TArray<AActor*> GetControllableActors();

	
};
