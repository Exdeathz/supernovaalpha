// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactable_Terminal.h"

constexpr TCHAR GPrompt[4] = TEXT("$> ");

// Sets default values for this component's properties
UInteractable_Terminal::UInteractable_Terminal()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UInteractable_Terminal::BeginPlay()
{
    Super::BeginPlay();
    UpdateText();
}

void UInteractable_Terminal::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

FString UInteractable_Terminal::GetScreenText() const
{
    TArray<FString> FullTerminal = Buffer;
	FullTerminal.Add(GPrompt + InputLine);

	// WrapLines
	TArray<FString> WrappedLines(WrapLines(FullTerminal));
	Truncate(WrappedLines);

	return JoinWithNewline(WrappedLines);
}

void UInteractable_Terminal::ActivateTerminal()
{
    UE_LOG(LogTemp, Warning, TEXT("Terminal Activated!"));
    FInputKeyBinding PressedBinding(EKeys::AnyKey, EInputEvent::IE_Pressed);
    PressedBinding.KeyDelegate.BindDelegate(this, &UInteractable_Terminal::OnKeyDown);

    FInputKeyBinding RepeatBinding(EKeys::AnyKey, EInputEvent::IE_Repeat);
    RepeatBinding.KeyDelegate.BindDelegate(this, &UInteractable_Terminal::OnKeyDown);

	if (GetOwner()->InputComponent == nullptr) return;
    Activated = true;
    UE_LOG(LogTemp, Warning, TEXT("Terminal Keybindings added!"));

    PressedBindingIndex = GetOwner()->InputComponent->KeyBindings.Emplace(MoveTemp(PressedBinding));
    RepeatBindingIndex = GetOwner()->InputComponent->KeyBindings.Emplace(MoveTemp(RepeatBinding));

}

void UInteractable_Terminal::DeactivateTerminal() const
{
    
    if (GetOwner()->InputComponent == nullptr || !Activated) return;
    UE_LOG(LogTemp, Warning, TEXT("Terminal Deactivated!"));
	
	// Must do in this order as RepeatBindingIndex > PressedBindingIndex so would change when first is removed
	GetOwner()->InputComponent->KeyBindings.RemoveAt(RepeatBindingIndex);
	GetOwner()->InputComponent->KeyBindings.RemoveAt(PressedBindingIndex);
}

void UInteractable_Terminal::PrintLine(const FString &Line)
{
    FString Input = Line;
	FString Left, Right;
	while (Input.Split(TEXT("\n"), &Left, &Right))
	{
		Buffer.Emplace(Left);
		Input = Right;
	} 
	Buffer.Emplace(Input);
	UpdateText();

}

void UInteractable_Terminal::ClearScreen()
{
    Buffer.Empty();
	UpdateText();
}

void UInteractable_Terminal::StartInteract()
{
    ActivateTerminal();
}

void UInteractable_Terminal::OnKeyDown(FKey Key)
{
    if (Key == EKeys::Enter)
	{
		AcceptInputLine();
	}

	if (Key == EKeys::BackSpace)
	{
		Backspace();
	}

    const FString KeyString = GetKeyString(Key);
    const FModifierKeysState KeyState = FSlateApplication::Get().GetModifierKeys();
	if (KeyState.IsShiftDown() || KeyState.AreCapsLocked())
	{
		InputLine += KeyString.ToUpper();
	}
	else
	{
		InputLine += KeyString.ToLower();
	}

	UpdateText();

}

TArray<FString> UInteractable_Terminal::WrapLines(const TArray<FString> &Lines) const
{
    TArray<FString> WrappedLines;
	for (auto &&Line : Lines)
	{
		FString CurrentLine = Line;
		do
		{
			WrappedLines.Add(CurrentLine.Left(MaxColumns));
			CurrentLine = CurrentLine.RightChop(MaxColumns);
		}
		while (CurrentLine.Len() > 0);
	}
	return WrappedLines;

}

void UInteractable_Terminal::Truncate(TArray<FString> &Lines) const
{
    while (Lines.Num() > MaxLines)
	{
		Lines.RemoveAt(0);
	}
}


FString UInteractable_Terminal::JoinWithNewline(const TArray<FString> &Lines) const
{
    FString Result;
	for (auto &&Line : Lines)
	{
		Result += Line + TEXT(" <br>");
	}
	return Result;
}

void UInteractable_Terminal::AcceptInputLine()
{
    // NOTE! This is the one that takes the "Cartridge" component
    Buffer.Emplace(GPrompt + InputLine);
	auto Cartridge = GetOwner()->FindComponentByClass<UCartridge>();
	if (Cartridge != nullptr)
	{
		Cartridge->OnInput(InputLine);
	}
	InputLine = TEXT("");

}

void UInteractable_Terminal::Backspace()
{
    if (InputLine.Len() > 0)
	{
		InputLine.RemoveAt(InputLine.Len()-1);
	}
}

FString UInteractable_Terminal::GetKeyString(FKey Key) const
{
    const uint32* KeyCode = nullptr;
	const uint32* CharCode = nullptr;
	FInputKeyManager::Get().GetCodesFromKey(Key, KeyCode, CharCode);
	if (CharCode != nullptr)
	{
		ANSICHAR Char[2] = {static_cast<ANSICHAR>(*CharCode), '\0'};
		return ANSI_TO_TCHAR(Char);
	}

	return TEXT("");
}

void UInteractable_Terminal::UpdateText()
{
    TextUpdated.Broadcast(GetScreenText());
}