// Fill out your copyright notice in the Description page of Project Settings.


#include "Cartridge.h"

// Sets default values for this component's properties
UCartridge::UCartridge()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCartridge::BeginPlay()
{
	Super::BeginPlay();
	Terminal = GetOwner()->FindComponentByClass<UInteractable_Terminal>();
	checkf(Terminal, TEXT("[%s]: No Terminal found"), *GetName())

	// ...
	
}


// Called every frame
void UCartridge::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


void UCartridge::PrintLine(const FString &Line) const
{
	Terminal->PrintLine(Line);

}

void UCartridge::PrintLine(const TCHAR *Line) const
{
	Terminal->PrintLine(Line);

}

void UCartridge::ClearScreen() const
{
	Terminal->ClearScreen();
}