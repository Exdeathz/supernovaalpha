// Fill out your copyright notice in the Description page of Project Settings.


#include "TerminalContolled.h"

// Sets default values for this component's properties
UTerminalContolled::UTerminalContolled()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTerminalContolled::BeginPlay()
{
	Super::BeginPlay();
	ClosedLocation = GetOwner()->GetActorLocation();

	// ...
	
}


// Called every frame
void UTerminalContolled::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTerminalContolled::Open()
{
	GetOwner()->SetActorLocation(OpenLocation);

}

void UTerminalContolled::Close()
{
	GetOwner()->SetActorLocation(ClosedLocation);

}
