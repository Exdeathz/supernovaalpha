// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//include what you use
#include "Interactable_Terminal.h"
//--

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Cartridge.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SUPERNOVA_API UCartridge : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCartridge();
	virtual void OnInput(const FString& Input) PURE_VIRTUAL(UCartridge::OnInput,);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void PrintLine(const FString& Line) const;
	void PrintLine(const TCHAR* Line) const; // Avoid template for this case.
	template<SIZE_T N, typename ...Types>
	void PrintLine(const TCHAR (&Fmt)[N], Types... Args) const
	{
		PrintLine(FString::Printf(Fmt, Args...));
	}
	void ClearScreen() const;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:

class UInteractable_Terminal* Terminal;

	

		
};
