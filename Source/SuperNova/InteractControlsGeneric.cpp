// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractControlsGeneric.h"
#include "Interactable_Generic.h"


// Sets default values for this component's properties
UInteractControlsGeneric::UInteractControlsGeneric()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInteractControlsGeneric::BeginPlay()
{
	Super::BeginPlay();

	Input = GetOwner()->FindComponentByClass<UInputComponent>();
	if (Input)
	{
		Input->BindAction("Interact", IE_Pressed, this, &UInteractControlsGeneric::Interact);

	}

	// ...
	
}


// Called every frame
void UInteractControlsGeneric::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UInteractControlsGeneric::Interact()
{
	FHitResult InteractingObject = GetNearestInteractibleObject();
	if (InteractingObject.GetActor() != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Hit Object = %s"), *InteractingObject.GetActor()->GetName());
		if( InteractingObject.GetActor()->FindComponentByClass<UInteractable_Generic>() ) 
		{
			InteractingObject.GetActor()->FindComponentByClass<UInteractable_Generic>()->StartInteract();
			//InteractingObject.GetActor()->FindComponentByClass<UTextRenderComponent>()->SetText(TEXT("Interaction Complete!\nThis should be on a new line!"));

		}


	}


}

FHitResult UInteractControlsGeneric::GetNearestInteractibleObject()
{

	FVector PlayerViewpointLocation;
	FRotator PlayerViewpointRotation;
	GetWorld()->GetFirstPlayerController()->GetActorEyesViewPoint(
		OUT PlayerViewpointLocation,
		OUT PlayerViewpointRotation
	);

	FVector LineTraceEnd = PlayerViewpointLocation + PlayerViewpointRotation.RotateVector(FVector(Reach, 0.0f, 0.0f));
	FHitResult InteractableObject;
	FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner());

	GetWorld()->LineTraceSingleByObjectType(
		InteractableObject,
		PlayerViewpointLocation,
		LineTraceEnd,
		FCollisionObjectQueryParams(ECC_TO_BITFIELD(ECC_PhysicsBody)),
		TraceParams
	);

	return InteractableObject;

}

