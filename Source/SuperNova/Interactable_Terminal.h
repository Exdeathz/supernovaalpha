// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//NOTE! in the MyProject\Source\MyProject\ directory you need to  add "SlateCore", "Slate", in the PublicDependencyModuleNames section. https://forums.unrealengine.com/development-discussion/c-gameplay-programming/47821-why-does-bindkey-cause-link-error
//include what you use
#include "GameFramework/Actor.h"
#include "Components/ActorComponent.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Framework/Application/SlateApplication.h"
#include "Framework/Commands/InputChord.h"
#include "Cartridge.h"
//--

#include "CoreMinimal.h"
#include "Interactable_Generic.h"
#include "Interactable_Terminal.generated.h"

/**
 * 
 */

//Start by declaring a delegate (event) that I can use in blueprint. This will allow me to link up the terminal interaction...
//    To the actual text object more easily.

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTextUpdateSignature, FString, Text);
struct FKey;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class SUPERNOVA_API UInteractable_Terminal : public UInteractable_Generic
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInteractable_Terminal();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintAssignable, Category = "Terminal")
	FTextUpdateSignature TextUpdated;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FString GetScreenText() const;

	UFUNCTION(BlueprintCallable)
	void ActivateTerminal();

	UFUNCTION(BlueprintCallable)
	void DeactivateTerminal() const;

	void PrintLine(const FString& Line);
	void ClearScreen();

	virtual void StartInteract();

private:
	void OnKeyDown(FKey Key);
	TArray<FString> WrapLines(const TArray<FString>&  Lines) const;
	void Truncate(TArray<FString>& Lines) const;
	FString JoinWithNewline(const TArray<FString>& Lines) const;
	void AcceptInputLine();
	void Backspace();
	FString GetKeyString(FKey Key) const;
	void UpdateText();


	UPROPERTY(EditAnywhere)
	int32 MaxLines = 10;

	UPROPERTY(EditAnywhere)
	int32 MaxColumns = 70;

	TArray<FString> Buffer;
	FString InputLine;

	int32 PressedBindingIndex;
	int32 RepeatBindingIndex;
	
	bool Activated = false;
};
