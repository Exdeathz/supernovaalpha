// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactable_Generic.h"

// Sets default values for this component's properties
UInteractable_Generic::UInteractable_Generic()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInteractable_Generic::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UInteractable_Generic::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UInteractable_Generic::StartInteract()
{
	//default implementation
	GetOwner()->FindComponentByClass<UTextRenderComponent>()->SetText(TEXT("Interaction Complete!\nThis should be on a new line!"));
}

