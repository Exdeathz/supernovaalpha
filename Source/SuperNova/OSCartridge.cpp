// Fill out your copyright notice in the Description page of Project Settings.

#define OUT
#include "OSCartridge.h"


void UOSCartridge::BeginPlay()
{
    Super::BeginPlay();

}

void UOSCartridge::OnInput(const FString &Input)
{
    if (Input == TEXT("ls /objects"))
    {
        TArray<AActor*> FoundActors;
        UGameplayStatics::GetAllActorsOfClass(GetWorld(), AStaticMeshActor::StaticClass(), FoundActors);

        for (auto Pawn : FoundActors)
        {
            if (Pawn != nullptr){
                PrintLine(Pawn->GetName());
            }
            
        }
    }

    if (Input == TEXT("help"))
    {
        PrintLine("ls");
    }

    else
    {
        ParseCommand(Input);
    }
    

}

void UOSCartridge::ParseCommand(const FString &Input)
{
    FString left, right = TEXT("");
    if(Input.Split(" ", OUT &left, OUT &right)){

    UE_LOG(LogTemp, Warning, TEXT("Left side: %s\nRight side: %s"), *left, *right);

    ExecCommand(left, right);
    }
    else{ExecCommand(Input, right);}


}

void UOSCartridge::Command_OpenDoor(const FString &Input)
{
    TArray<AActor*> ControllableActors = GetControllableActors();

    for (auto Actor : ControllableActors)
    {
        if (Actor == nullptr){ continue; }
        if(Actor->GetName() == Input){Actor->FindComponentByClass<UTerminalContolled>()->Open();}
    }

}

void UOSCartridge::List(const FString &Input)
{
    UE_LOG(LogTemp, Warning, TEXT("Listing based on input args: %s"), *Input);
    TArray<AActor*> ControllableActors = GetControllableActors();
    for (auto Actor : ControllableActors)
    {
        if ( Actor == nullptr){ continue;}
        PrintLine(Actor->GetName());

    }


}

void UOSCartridge::ExecCommand(const FString &Command, const FString &Args)
{
    if (Command == FString(TEXT("ls"))) {List(Args);}
    if (Command == FString(TEXT("open"))) {Command_OpenDoor(Args);}
}

TArray<AActor *> UOSCartridge::GetControllableActors()
{
    TArray<AActor*> FoundActors;
    TArray<AActor*> FilteredActors;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), AActor::StaticClass(), FoundActors);

    for (auto Actor : FoundActors)
    {
        if (Actor == nullptr){ continue; }
        if (Actor->FindComponentByClass<UTerminalContolled>() == nullptr){continue;}
        FilteredActors.Emplace(Actor);

        
    }
    return FilteredActors;

}